# Kinematic PPP with rtklib

Kinematic precise point positions (kPPP) is a method for precise positioning of a single moving dual frequency GNSS receiver. See:

This document describes how to use the open source `rtklib` toolkit to do kPPP processing. The normal use case is for a GNSS base station on drifting sea ice. Another use case is a roving receiver used for a 'rapid static' survey of ground control points.

## rtklib

RTKlib is downloadable here: http://www.rtklib.com/

Once installed, the relevant program for kPPP is `rtkpost`.

## extracting RINEX data

Leica MDB/DBX files can be converted to RINEX using the legacy teqc application: https://www.unavco.org/software/data-processing/teqc/teqc.html

Be sure to extract both observaiton (.obs) and navigation files (.n). Use a command like:

`teqc -leica mdb +nav rinex_export_path\rinexfilename.n rawdata_path\leicafile.dbx > rinex_export_path\rinexfilename.obs`

This ensures that a quick 'navigation solution' can be determined on ship, without access to precise satellite orbit data.

## On-ship 'nav solutions'

`rtklib` is used to process a quick navigation-grade solution from RINEX observations and navigation data recorded on the GNSS receiver.

Using `rtkpost.exe`, set up for kinematic PPP processing with the RINEX files made in the step above. No extra downloads are needed.

1. select input files in the `rtkpost` GUI form.
2. select an output file
3. press `options`.
4. in setting 1, choose `PPP kinematic` as the processing mode, `combined` as filter type, `Iono-free LC` as ionosphere correction, and check both `GPS` and `GLO` boxes.
5. in Output tab, set options as you prefer.
6. click OK
7. In the main `rtkpost` window, press `execute`

This will get a .pos file with positions accurate to a few metres - like a regular handheld GPS.

## precise processing

Using high end GNSS recievers we can get to decimeter-level positioning if we give `rtkpost` extra information about precise satellite orbits.

### Ancillary data

kPPP relies on precise orbit and clock products for GNSS satellites. These are obtainable here (you will need a NASA earthdata login):

https://cddis.nasa.gov/archive/gnss/products/

Find the GPS week for observations, using the calendar here:  https://www.gnsscalendar.com/





### Tips

Always use combined processing (forward and reverse). This helps to iron out any unusual positions discovered in a single pass through the data.

## Deployment hints

kPPP positions take time to 'settle' - ensure that receivers are running for at least 30 minutes before they are needed for precise position, and avoid loss-of-lock conditions. A kPPP solution can still be reached with shorter timeframes, although it will be less accurate.
