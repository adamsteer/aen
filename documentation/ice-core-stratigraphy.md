## Ice core stratigraphy profiling

**Adam Steer**

Stratigraphic analysis of Nansen Legacy sea ice cores is done using a method adopted from the Australian Antarctic Division. Cores are sliced in half along their length, then a thin section is cut on the bandsaw from one half of the core - approximately 3-4 mm thick. These sections are cleaned with a brush and a knife, then placed on a cross polarising light table.

### Setting up the lab

- prepare the bandsaw

### Cutting cores in half
- ensure the bandsaw is clean when starting
- set the cutting guide to 4.5 cm, and slice all core sections along their length resulting in two 'half cores'
- avoid twisting the core during cutting. if need be, prepare one side of the core with a small flat surface which will sit on the saw bench
- *Take care to preserve the orientation of core segments!*
- clean the bandsaw cutting area again, ensuring there is plenty of room for ice 'sawdust' to escape
- clean the cut surface of the core with a brush, then scrape with a knife or scraping razor to remove as many saw marks as possible.
- also ensure that the edges of the core are smooth and unlikely to snag while cutting.

#### Preparing thin sections
- before cutting, ensure that the area for ice sawdust to collect under the cutting bench is clean. If ice shavings cannot escape, the section will probably be difficult to cut.
- set the cutting guide to 3.5 mm (marked on cutting bench)
- placing the cut edge of the core against the saw cutting guide, carefully slice a thin ice section
- use a wooden brush handle (or other wooden tool) to push ice toward the blade, ensuring fingers are clear. Watch closely as you cut to ensure an even slice
- keep gentle pressure on the core against the cutting guide until the entire segment has been cut.
- if it breaks, *stay calm* - you have plenty of material left. Keep track of broken pieces for assembly on the light table.
- **turn the bandsaw off as soon as the section is cut**
- carefully move the core and thin section (use the core material to support it) away from the bandsaw
- gently remove the thin section. If it breaks, *stay calm* - you have plenty of material left. Keep track of broken pieces for assembly on the light table.
- if possible, clean up the new cut side of the thin slice. Use a brush, and the sharp scraping blade
- assemble on the light table
- repeat for all core segments.

#### Photographing the ice core

- set the ice lab canon to a high ISO (1600) and use P mode. Set the lens to 50 mm.
- photograph the core label
- move along the core collecting images with at least 50% overlap.
- include the ruler in imagery, try to balance exposure so that numbers are just visible
- photograph the core label again to indicate the end of this core.
- if the core is longer than 1 m, make sure the 'last section' of the top 1 m is photographed with a lot of overlap on the next 1 m. See diagram below (TBD)

Sometimes the core sections will lose thickness by sublimation if left on the light table with the light turned on (heat source from below). This may help with clear images, at the cost of losing ice at core section edges. If you do this **remember to take the top sheet off the light table every day to avoid ice accumulating on it**

### Post cutting cleanup

- remove the top sheet from the polarizing light table, keeping it flat, and place flat on a bench taking care to avoid scratching it
- dispose of thin sections
- clean the bandsaw thoroughly, removing built up ice shavings


## Postprocessing

#### Core reconstruction

Images are stitched using the open source Hugin Panorama stitcher, to reconstruct an entire core. This is a quite manually intensive approach, used because it provides the most control over accurate reconstruction.

- Back up raw imagery from the ice lab camera
- using rawtherapee (or other raw image processor), export jpeg imagery for reconstruction.
- load jpeg imagery into Hugin
- generate control points
- manually inspect control points
- set optimiser to `custom parameters`
- in the optimiser set ..., ....,
- optimise imagery and open the fast panorama previewer
- iteratively adjust, add and correct control points as needed - optimising the panorama after each adjustment until it looks like a straight ice core.

#### plotting parameters with core Images

Reconstructed core images are cleaned up and have the background removed with photo editing software.

Code for plotting sea ice parameters is given here: https://gitlab.com/adamsteer/aen/-/blob/main/notebooks/icecore-plot-tests.ipynb

Plots are exported as `.svg`, and reassembled with scaled core images in an illustration program (Inkscape, Illustrator)
