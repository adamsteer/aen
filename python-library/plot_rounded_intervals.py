from matplotlib import pyplot as plt
import numpy as np
import matplotlib.ticker as plticker
from matplotlib.ticker import MultipleLocator
from scipy.interpolate import Akima1DInterpolator, PchipInterpolator, CubicSpline

def plot_rounded_intervals(
    intervals,
    values,
    ax=None,
    orientation="horizontal",
    color="k",
    rounding=0.1,
    baseline=0,
    **kwargs
):
    """
    From Morgan Williams:
    https://gist.github.com/morganjwilliams/fbdcef72e8d11b46b6e0a817a9c5afcc


    Plot a step profile plot with rounded corners.
    Parameters
    ----------
    intervals : numpy.ndarray
        Array of interval/bin edges.
    values : numpy.ndarray
        Values of properties for the intervals/bins.
    ax : matplotlib.axes.Axes
        Axis to plot on, if not creating a new one.
    orientation : str
        Which orientation in which to make the plot.
    color : str
        Color for the line plot.
    rounding : float | tuple
        Amount of rounding to apply to the profile corners. Where a tuple is provided, it
        corresponds to the scale along the interval axis, and the scale along the value axis.
    baseline : float
        The 'zero point' of the profile, which it will return to on either end.
    Returns
    -------
    matplotlib.axes.Axes

    """
    if ax is None:
        fig, ax = plt.subplots(1)
        ax.invert_yaxis()

    if rounding is None:
        rounding = 0
    if not isinstance(rounding, tuple):
        rounding = (rounding, rounding)

    # generate interval midway points and plot markers. Set Zorder high to plot on top of lines coming net
    pts = (intervals[1:] + intervals[:-1]) / 2
    if orientation == "horizontal":
        ax.scatter(values, pts, marker="o", c="w", edgecolor=color, zorder=10 )
    else:
        ax.scatter(pts, values, marker="o", c= "w", edgecolor=color, zorder=10 )

    # to some extent, this might be useful - but gives non-rounded edges
    if np.isclose(rounding[0], 0):
        ax.stairs(
            values, edges=intervals, orientation=orientation, color=color, **kwargs
        )
    else:
        # bevel the corners off the step profile
        #make an array which repeats the intervals 4 times each
        ir = np.repeat(intervals, 4).astype(float)

        #print("intervals: {}".format(ir))

        #make another array starting and ending at [0,0], with repeated *values*
        vr = np.hstack(
            [[baseline, baseline], np.repeat(values, 4), [baseline, baseline]]
        ).astype(float)

        #print("values: {}".format(vr))

        # get the difference in sign between each value pair - is each step
        # a value increase or decrease?
        sign = np.sign(np.diff(np.hstack([[baseline], values, [baseline]])))

        #print(sign)

        # corner rounding - generate some points along which to interpolate a plotting line
        # the next section sets Y values for the eventual curve
        # start at element 2 - beginning of real values, put an intermediate step in one direction (+ or -)
        # maximum 1/4 of the step height up/down
        vr[2::4] -= sign * np.min(
            np.abs(
                np.vstack(
                    [
                        np.ones(intervals.size) * rounding[1],
                        np.hstack(
                            [
                                [sign[0] * rounding[1]],
                                np.diff(values) / 4,
                                [sign[-1] * rounding[1]],
                            ]
                        ),
                    ]
                )
            ),
            axis=0,
        )

        #at element 5, put an intermeduate step on the other direction
        vr[5::4] += sign[1:] * np.min(
            np.abs(
                np.vstack(
                    [
                        np.ones(intervals.size - 1) * rounding[1],
                        np.hstack(
                            [
                                [sign[1] * rounding[1]],
                                np.diff(values)[1:] / 4,
                                [sign[-1] * rounding[1]],
                            ]
                        ),
                    ]
                )
            ),
            axis=0,
        )

        #print("values 2: {}".format(vr))

        # here we set X values for intermediate points
        # maximum 1/4 of the bin width
        interval_offsets = np.min(
            np.vstack(
                [np.ones(intervals.size - 1) * rounding[0], np.diff(intervals) / 4]
            ),
            axis=0,
        )

        #print(interval_offsets)

        ir[3:-1:4] += interval_offsets
        ir[4::4] -= interval_offsets  # maximum half of the bin width

        #print(ir[4::4])

        # interpolation - construct a line to plot along the points made above
        _xs = np.linspace(0, ir.size, int(10 * (ir.size / rounding[0])))

        #print(_xs)

        i_interp = PchipInterpolator(np.arange(ir.size), ir)
        v_interp = PchipInterpolator(np.arange(ir.size), vr)
        #i_interp = Akima1DInterpolator(np.arange(ir.size), ir)
        #v_interp = Akima1DInterpolator(np.arange(ir.size), vr)
        #i_interp = CubicSpline(np.arange(ir.size), ir)
        #v_interp = CubicSpline(np.arange(ir.size), vr)
        # plotting

        if orientation == "horizontal":
            ax.plot(v_interp(_xs), i_interp(_xs), color=color, **kwargs)
        else:
            ax.plot(v_interp(_xs), i_interp(_xs), color=color, **kwargs)

    return ax
