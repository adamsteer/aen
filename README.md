# Arven Etter Nansen sea ice processing

This repository holds documentation and processing code for sea ice physical properties work undertaken in the Nansen Legacy Project (Arven Etter Nansen), Research Council of Norway project #287871 . This work is primarily taking place at the Norwegian Polar Insitute (https://npolar.no).

## Whats here

[**anafi-operations**](anafi-operations) contains user guides, an operations manual, checklists, and flight tips for using NPI's ANAFI USA drone on sea ice and from *FF Kronprins Haakon*

[**ancillary-files**](ancillary-files) contains parts used in some processes, for example text block templates used for assembling processed output data.

[**documentation**](documentation) generally contains 'how to' documents for processes used before getting to using any code contained in this repository - for example how to use RTKlib for GNSS processing.

[**helper-scripts**](helper-scripts) contains some shell programs and [PDAL](https://pdal.io) pipelines. It has its own `README`

[**mapdata**](mapdata) contains data used to assemble maps in some of the Jupyter notebooks this repository contains

[**jupyter-notebooks**](jupyter-notebooks) Jupyter notebooks used for various data processing and visualisation tasks. Each is self-dcoumented.

[**python-library**](python-library) is a place to put code that is used repeatedly in notebooks.
