#!/bin/bash
# shell script to extract all the useful metadata parts from Parrot ANAFI USA drone images,
# and assemble them into a CSV file.

# a little help

helptext="Please run like: \
#> sh getcameracentres.sh ./directory_of_images JPG outputfile.csv\n
\n
...where input images have the file extension .JPG, and are stored in ./directory_of_images.\n
The general usage pattern is:\n
#> getcameracentres.sh input_directory extension output_csv_file\n
Please adapt it to your needs."

# assign useful variable names
image_directory=$1
extension=$2
output_csv_file=$3

# write the help function
test $# -gt 0 || { echo $helptext; exit 1; }

# the processing part! Here is where to change what tags you want, and also modify the floating point precision of output if you want
exiftool -datetimeoriginal -subsectimeoriginal -gpslatitude -gpslongitude -gpsaltitude -abovegroundaltitude -gpsxyaccuracy -gpszaccuracy -camerapitchdegree -camerarolldegree -camerayawdegree $image_directory/*.$extension -c "%.8f" -n -csv > $output_csv_file

